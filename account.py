# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.wizard import Wizard, StateTransition, StateReport
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.pyson import Eval, Id


class Journal(metaclass=PoolMeta):
    __name__ = 'account.payment.journal'

    receipt_report = fields.Many2One('ir.action.report', 'Receipt report',
        domain=[
            ('report_name', '=', 'account.payment.receipt')])
    receipt_sequence = fields.Many2One('ir.sequence', 'Receipt Sequence',
        domain=[
            ('sequence_type', '=', Id('account_payment_receipt_type',
                'sequence_type_account_payment_journal'))])


class PrintReceipt(Wizard):
    """Print payment receipt"""
    __name__ = 'account.payment.receipt.print'

    start = StateTransition()
    print_ = StateReport('account.payment.receipt')

    def transition_start(self):
        Payment = Pool().get('account.payment')
        payment = Payment(Transaction().context['active_id'])

        if not payment.journal.receipt_report:
            raise UserError(gettext(
                'account_payment_receipt_type'
                '.msg_account_payment_receipt_print_no_report',
                journal=payment.journal.rec_name))
        return 'print_'

    def do_print_(self, action):
        pool = Pool()
        Payment = pool.get('account.payment')
        payment = Payment(Transaction().context['active_id'])
        ActionReport = pool.get('ir.action.report')
        Action = pool.get('ir.action')

        if not payment.journal.receipt_report:
            raise UserError(gettext(
                'account_payment_receipt_type'
                '.msg_account_payment_receipt_print_no_report',
                journal=payment.journal.rec_name))

        action_id = payment.journal.receipt_report

        action_report = ActionReport(action_id)
        action = action_report.action
        action = Action.get_action_values(action.type, [action.id])[0]
        return action, {
            'ids': Transaction().context['active_ids']
        }


class Payment(metaclass=PoolMeta):
    __name__ = 'account.payment'

    origin_lines = fields.Function(
        fields.Many2Many('account.move.line', None, None, 'Origin lines'),
        'get_origin_lines')
    receipt_number = fields.Char('Receipt number',
        states={
            'readonly': Eval('state').in_(['failed', 'succeeded']),
            })

    def get_origin_lines(self, name=None):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        InvoiceLine = pool.get('account.invoice.line')

        checked_reconc_ids = set()
        checked_origins = set()

        def _get_reconciliation_origin(line):
            _lines = []
            origin = line.origin or line.move.origin
            if origin and isinstance(origin, (Invoice, InvoiceLine)):
                _lines.append(line)
            for move_line in line.move.lines:
                if (not move_line.reconciliation
                        or move_line.reconciliation.id in checked_reconc_ids):
                    continue
                if (move_line == line
                        or move_line.reconciliation == line.reconciliation):
                    continue
                checked_reconc_ids.add(move_line.reconciliation.id)
                for move_recol in move_line.reconciliation.lines:
                    _lines.extend(_get_reconciliation_origin(move_recol))

            return _lines

        lines = _get_reconciliation_origin(self.line)
        values = []
        # now we must have a line per origin, the oldest one
        for line in sorted(lines,
                key=lambda l: (l.origin or l.move.origin, l.date)):
            origin = line.origin or line.move.origin
            if origin in checked_origins:
                old_line, = [l for l in values if l.origin == origin]
                if old_line.date < line.date:
                    continue

                values.remove(old_line)

            checked_origins.add(origin)
            values.append(line)
        return list(set([r.id for r in values])) or [self.line.id]

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        PaymentJournal = pool.get('account.payment.journal')
        for vals in vlist:
            journal_id = vals.get('journal', None)
            if journal_id:
                journal = PaymentJournal(journal_id)
                if journal.receipt_sequence:
                    vals['receipt_number'] = journal.receipt_sequence.get()
        return super().create(vlist)


class PaymentProcessing(metaclass=PoolMeta):
    __name__ = 'account.payment'

    def create_processing_move(self):
        move = super().create_processing_move()
        if self.receipt_number:
            suffix = getattr(move, 'description', '')
            move.description = '[%s] %s' % (self.receipt_number, suffix)
            for line in move.lines:
                suffix = getattr(line, 'description', '')
                line.description = '[%s] %s' % (self.receipt_number, suffix)
        return move


class PaymentGroup(metaclass=PoolMeta):
    __name__ = 'account.payment.group'

    origin_lines = fields.Function(
        fields.Many2Many('account.move.line', None, None, 'Origin lines'),
        'get_origin_lines')

    def get_origin_lines(self, name=None):
        lines = []
        for payment in self.payments:
            lines.extend(payment.get_origin_lines())
        return lines


class MoveLine(metaclass=PoolMeta):
    __name__ = 'account.move.line'

    origin_invoice = fields.Function(
        fields.Many2One('account.invoice', "Invoice"),
        'get_origin_invoice')

    def get_origin_invoice(self, name=None):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        InvoiceLine = pool.get('account.invoice.line')

        origin = self.origin or self.move.origin
        if not origin:
            return None
        if isinstance(origin, Invoice):
            return origin.id
        elif isinstance(origin, InvoiceLine):
            return origin.invoice.id
        return None
