datalife_account_payment_receipt_type
=====================================

The account_payment_receipt_type module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-account_payment_receipt_type/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-account_payment_receipt_type)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
